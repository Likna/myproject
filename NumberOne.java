//Считать массив из  n чисел. Проверить, что в нем существует 
//ровно три числа, в котором цифры идут по убыванию

import java.util.Scanner;
class NumberOne{
	public static void main(String[] args) {
		Scanner inp = new Scanner(System.in);
		System.out.println("Введите длину массива:"); 
		//длина массива - натуральное число
		int length = inp.nextInt();
		if(length>0){
		int[] arr = new int[length];
		int count = 0;
		int sot = 0; //сотни
		int desyat = 0; //десятки
		int ed = 0; //единицы

		System.out.println("Заполните массив:");

		for(int i = 0; i < length; i++){
			arr[i] = inp.nextInt();
		}

		for(int i = 0; i < length; i++){
			if(((arr[i]>=210) & (arr[i]<=987))|((arr[i]<=-210) & (arr[i]>=-987))){
				if(arr[i]>0){
				    sot = arr[i]/100;
			        desyat = (arr[i]/10)%10;
				    ed = arr[i]%10;
			    }else{
				    sot = -arr[i]/100;
				    desyat = -(arr[i]/10)%10;
				    ed = -arr[i]%10;				
			    }
				if((sot > desyat) && (desyat > ed)){
					count += 1;
				}
			}
		}
		System.out.println(count==3 ? "Такие 3 числа существуют":"Таких 3 чисел не существует");
		}
	}
}